use files;

use gtk;
use gtk::prelude::*;
use gtk::Builder;

use std::cell::RefCell;
use std::rc::Rc;
use std::vec::Vec;

use uuid::Uuid;
use std::str::FromStr;

use errors::*;

struct State {
    current_page: Option<Uuid>,
    page_list_map: Vec<Uuid> // workaround lack of sub-classing
}

impl State {
    fn new() -> State {
        let current_page = None;
        let page_list_map = Vec::new();

        State {
            current_page,
            page_list_map,
        }
    }
}

pub fn run() -> Result<()> {
    // application state
    let state = Rc::new(RefCell::new(State::new()));

    // init gtk or exit
    gtk::init().map_err(|_| "failed to init gtk")?;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Load External Resources
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////

    // build UI from XML definition
    let glade_src = include_str!("../assets/app.glade");
    let builder  = Builder::new_from_string(glade_src);

    // grab handles to some important widgets
    let main_window: gtk::ApplicationWindow = builder.get_object("main_window")
                                                     .ok_or("couldn't get main_window")?;
    let pages_list: gtk::ListBox = builder.get_object("pages_list")
                                                     .ok_or("couldn't get pages_list")?;
    let main_editor: gtk::TextView = builder.get_object("main_editor")
                                                     .ok_or("couldn't get main_editor")?;
    let save_button: gtk::Button = builder.get_object("save_button")
                                                     .ok_or("couldn't get save_button")?;
    let new_page_button: gtk::Button = builder.get_object("new_page_button")
                                              .ok_or("couldn't get new_page_button")?;

    // add css styles
    let css = gtk::CssProvider::new();
    css.load_from_path("assets/app.css").map_err(|_| "unable to load app css")?;
    let screen = main_window.get_screen().ok_or("unable to get screen for main window")?;
    gtk::StyleContext::add_provider_for_screen(
        &screen,
        &css,
        gtk::STYLE_PROVIDER_PRIORITY_APPLICATION
    );

    // store for using later in callbacks
    let moveable_builder = Rc::new(builder);

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Setup Callbacks
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////

    // exit when window closed
    main_window.connect_delete_event(|_,_| {
        gtk::main_quit();
        Inhibit(false) // still run default handler to del window (not that it matters)
    });

    // handle clicks on pages in pages list
    {
        let state = state.clone();

        pages_list.connect_row_activated(move |_,row| {
            let mut state = state.borrow_mut();
            let uuid = state.page_list_map[row.get_index() as usize];
            let selected_text = files::get_page_contents(
                &uuid.hyphenated().to_string()
            ).expect("could not read page file");

            let maybe_text_buffer = main_editor.get_buffer();
            maybe_text_buffer.expect("couldn't access editor buffer").set_text(&selected_text);

            state.current_page = Some(uuid);
        });
    }

    // handle save button
    {
        let state = state.clone();
        let builder = moveable_builder.clone();

        save_button.connect_clicked(move |_btn| {
            let state = state.borrow_mut();
            let main_editor: gtk::TextView = builder.get_object("main_editor")
                                                    .expect("couldn't get main_editor");
            let hamburger_menu_button: gtk::MenuButton = builder.get_object("hamburger_menu_button")
                                                    .expect("couldn't get hamburger_menu_button");

            // close menu
            hamburger_menu_button.set_active(false);

            match state.current_page {
                Some(uuid) => {
                    match main_editor.get_buffer() {
                        Some(buffer) => {
                            let text_maybe = buffer.get_text(
                                &buffer.get_start_iter(),
                                &buffer.get_end_iter(),
                                false
                            );
                            match text_maybe {
                                Some(text) => {
                                    files::save_page_contents(&uuid.hyphenated().to_string(), &text).unwrap();
                                },
                                None => println!("no text in buffer")
                            };
                        },
                        None => println!("no buffer to save")
                    }
                },
                None => println!("can't save nothing"),
            }
        });
    }

    // handle new page button
    {
        let state = state.clone();
        let builder = moveable_builder.clone();

        new_page_button.connect_clicked(move |_btn| {
            let pages_list: gtk::ListBox = builder.get_object("pages_list")
                                                    .expect("couldn't get pages_list");
            {
                let mut state = state.borrow_mut();

                let uuid = Uuid::new_v4();
                let uuid_str = uuid.hyphenated().to_string();

                state.page_list_map.insert(0, uuid);

                let label = gtk::Label::new("");
                pages_list.prepend(&label);

                // kinda hack-ish; save empty page
                files::save_page_contents(&uuid_str, "").expect("couldn't save new page");
            }

            {
                let row = pages_list.get_row_at_index(0).expect("couldn't get row that was just added");

                // new row must be manually shown
                row.show_all();

                // make new page active
                row.activate(); // note actiavte will re-mut-borrow state
            }
        });
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Load Data
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////

    // load page list
    {
        let mut state = state.borrow_mut();

        for page_file in files::list_all_pages()? {
            if let Ok(page_file) = page_file {
                let path = page_file.path();
                let uuid_str = path.file_stem().ok_or("found file with no name")?.to_string_lossy();
                let uuid = Uuid::from_str(uuid_str.as_ref()).unwrap();
                let first_line = files::get_page_first_line(&uuid_str)?;

                state.page_list_map.insert(0, uuid);

                let label = gtk::Label::new(first_line.as_ref());
                pages_list.prepend(&label);
            }
        }
    }

    // show first page if any pages
    {
        match pages_list.get_row_at_index(0) {
            Some(first_row) => { first_row.activate(); },
            None => (),
        }
    }

    // make everything visible
    main_window.show_all();

    gtk::main();

    Ok(())
}

