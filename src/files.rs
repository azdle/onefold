use app_dirs::*;
use std::path::PathBuf;
use std::fs::ReadDir;
use std::fs;

use errors::*;

const APP_INFO: AppInfo = AppInfo{name: "onefold", author: "Patrick Barrett"};

fn get_pages_dir() -> Result<PathBuf> {
    app_dir(AppDataType::UserData, &APP_INFO, "pages").chain_err(|| "unable to find pages dir")
}
    
pub fn list_all_pages() -> Result<ReadDir> {

    get_pages_dir()?.read_dir().chain_err(|| "unable to read pages dir")
}

pub fn get_page_contents(uuid: &str) -> Result<String> {
    use std::io::prelude::*;

    let page_path = {
        let mut path = get_pages_dir()?;
        path.push(uuid);
        path.set_extension("md");
        path
    };

    let mut file = fs::File::open(page_path).chain_err(|| "couldn't open page")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents).chain_err(|| "couldn't read page")?;

    Ok(contents)
}

pub fn get_page_first_line(uuid: &str) -> Result<String> {
    use std::io::prelude::*;
    use std::io::BufReader;

    let page_path = {
        let mut path = get_pages_dir()?;
        path.push(uuid);
        path.set_extension("md");
        path
    };

    let file = fs::File::open(page_path).chain_err(|| "couldn't open page")?;
    let mut reader = BufReader::new(file);
    let mut line = String::new();
    reader.read_line(&mut line).chain_err(|| "couldn't read page")?;

    line = line.trim().to_owned();

    Ok(line)
}

pub fn save_page_contents(uuid: &str, contents: &str) -> Result<()> {
    use std::io::prelude::*;

    let page_path = {
        let mut path = get_pages_dir()?;
        path.push(uuid);
        path.set_extension("md");
        path
    };

    let mut file = fs::File::create(page_path).chain_err(|| "couldn't open page")?;
    file.write_all(contents.as_bytes()).chain_err(|| "couldn't write page")?;

    Ok(())
}
