extern crate gtk;
extern crate app_dirs;
#[macro_use]
extern crate error_chain;
extern crate uuid;

mod app;
mod files;

mod errors {
    error_chain!{}
}

fn main() {
    if let Err(ref e) = app::run() {
        use std::io::Write;
        let stderr = &mut ::std::io::stderr();
        let stderrerr = "couldn't write to stderr";

        writeln!(stderr, "error: {}", e).expect(stderrerr);

        for e in e.iter().skip(1) {
            writeln!(stderr, "because: {}", e).expect(stderrerr);
        }

        if let Some(backtrace) = e.backtrace() {
            writeln!(stderr, "backtrace: {:?}", backtrace).expect(stderrerr);
        }

        ::std::process::exit(1);
    }
}
